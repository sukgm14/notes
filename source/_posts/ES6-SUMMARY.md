---
title: ECMAScript 2015(ES6) 첫 경험, 개요편
date: 2019-04-15 13:47:03
tags: #ES6, #ECMAScript6, #ECMAScript2015, #ES 2015
---

![](/notes/image/es6/es2015-logo.png)

vue.js 학습하면서 ES6를 접하게 되었을 때 새로운 스크립트 언어인 줄만 알았다. 
javascript 언어의 표준이라고 하는데 코드를 보면 이해되는듯 하면서 어색한 부분도 많았고, 
이미 javascript에 대한 기본적인 문법은 알고 있으니 ES6의 문법과 비교하여
흥미로운 부분이 꽤 많았다.

spread operator, Destructuring, Arrow function 등등...

vue.js를 통해 ES6를 사용하게되면서 아래와 같은 대략적인 로드맵이 그려졌다. 
개인적이고 대략적인 사항이다.

- Front-End : vue.js, javascript(ECMAScript)...
- Back-End : java, node.js, sql... 

우선 [인프런 무료 es6-강좌-자바스크립트(윤지수)](https://www.inflearn.com/course/es6-%EA%B0%95%EC%A2%8C-%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8/) 를 보면서 익힌 기본 문법에 대해서 정리 하고자 한다.


# ECMAScript 2015(ES6) 무엇인가?

### ECMAScript 
  - Ecma 인터내셔널의 ECMA-262 기술 규격에 정의된 표준화된 스크립트 프로그래밍 언어 
    (쉽게 말해서 JavaScript 언어를 정의하는 국제 표준 이름) 
  - JavaScript는 Oracle의 상표
  - ES5를 포함하는 그 이전 세대의 자바스크립트를 많이 개선한 버전
  - 현재 ES6를 지원하지 않는다고 해도 ES5 트랜스컴파일을 통해 ES6 프로그램을 지원 가능


### ECMA 인터내셔널 : 표준화 기구
   - ECMA-262 : ECMAScript 언어 규격
   - ECMA-334 : C# 언어 규격
   - ECMA-388 : OOXML 규격


### History
  - 2000 : ES4, 첫 번째 시도
  - 2003-4 : E4X, ECMAScript를 위한 XML 확장
  - 2005-7 : ES4, 두 번째 시도
  - 2008 : ES4 버려짐
  - 2009 : ES5, "use strict", JSON, Object.create, accessors, etc
  - 2011 : ES5.1, 오류 수정. ISO/IEC 16262와 함께 출판
  - 2015 : ES6, ES6 Harmony. 
  - 2016 : ES7. 제곱 연산자(**) 등장
  - 2017 : ES8. Promise(async, await)





