---
title: ECMAScript 2015(ES6) 첫 경험, 기본문법 요약(1)
date: 2019-04-17 10:07:10
tags: #ES6, #ECMAScript6, #ECMAScript2015, #ES 2015
---

## ECMAScript 2015(ES6) 기본문법 요약(1)

### 새로운 메서드 

### for-of 반복문

### spread operator

### from 메서드

### Destructuring (Array, Object, JSON, 이벤트 객체전달)